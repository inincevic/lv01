package com.rplv;


import Zadatak1.BadStudent;
import Zadatak1.GoodStudent;
import Zadatak2.EngineeringStudent;
import zadatak3.Student_zad3;

public class Main {

    public static void main(String[] args) {
        /*
        //Zadatak 1
        GoodStudent goodStudent = new GoodStudent();
        BadStudent badStudent = new BadStudent();

        goodStudent.intro();
        goodStudent.learn();
        goodStudent.sleep();

        badStudent.intro();
        badStudent.learn();
        badStudent.sleep();

        //Zadatak 2
        EngineeringStudent engineeringStudent = new EngineeringStudent();
        engineeringStudent.watchTv();
        engineeringStudent.sleep();
        engineeringStudent.printYearAndCollegeName();
        */
        //Zadatak 3
        Student_zad3 student = new Student_zad3();
        student.setName("Pero");
        System.out.println(student.getName());
    }
}
