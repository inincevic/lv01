package Zadatak1;

public class GoodStudent extends Student{
    @Override
    public void sleep() {
        System.out.println("Dobar student spava 8 sati na noc");
    }

    @Override
    public void learn() {
        System.out.println("Dobar student uci 2 sata dnevno");
    }
}
