package Zadatak2;

public class EngineeringStudent implements Human, Student{
    @Override
    public void watchTv() {
        System.out.println("Student ne gleda televiziju");
    }

    @Override
    public void sleep() {
        System.out.println("Student spava 8 sati na noc");
    }

    @Override
    public void printYearAndCollegeName() {
        System.out.println("Year is " + year);
        System.out.println("College name is " + collegeName);
    }
}
